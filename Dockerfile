FROM golang:alpine
WORKDIR /compliance/license-bot
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 && go build check.go

ARG giteePullRequestIid
ARG giteeSourceBranch
ARG giteeSourceRepoHttpUrl
ARG GITEE_TOKEN
ARG giteeTargetNamespace
ARG giteeTargetRepoName

ENV giteePullRequestIid=${giteePullRequestIid} \
    giteeSourceBranch=${giteeSourceBranch} \
    giteeSourceRepoHttpUrl=${giteeSourceRepoHttpUrl} \
    GITEE_TOKEN={GITEE_TOKEN} \
    giteeTargetNamespace={giteeTargetNamespace} \
    giteeTargetRepoName={giteeTargetRepoName}

# 不能用 CMD []这种形式，这种方式读取不到变量
CMD ./check licenseScan ${giteePullRequestIid}  ${giteeSourceBranch} ${giteeSourceRepoHttpUrl} ${GITEE_TOKEN} ${giteeTargetNamespace}  ${giteeTargetRepoName}

ENTRYPOINT []