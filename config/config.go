package config

import (
	"github.com/go-yaml/yaml"
	"io/ioutil"
	"log"
)

var (
	OwnerWhiteList *OwnerList
	RepoWhiteList  *RepoList
)

type PullRepo struct {
	Action string `json:"action"`
	Repo   string `json:"repo"`
	Branch string `json:"branch"`
}

type ScanResult struct {
	Pass          bool   `json:"pass"`
	Synchronous   bool   `json:"synchronous"`
	ReportFlag    string `json:"report_flag"`
	ReportSummary string `json:"report_summary"`
	ReportUrl     string `json:"report_url"`
}

type OwnerList struct {
	Owners []string `yaml:"owners"`
}

type RepoList struct {
	Repos []string `yaml:"repos"`
}

type WhiteList struct {
	OwnerList *OwnerList `yaml:"ownerList"`
	RepoList  *RepoList  `yaml:"repoList"`
}

func LoadConfig(file string) error {
	var err error
	config, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	conf := &WhiteList{}

	err = yaml.Unmarshal(config, conf)
	if err != nil {
		log.Fatal(err)
	}

	if conf.RepoList != nil {
		conf.RepoList = &RepoList{}
	}
	if conf.OwnerList != nil {
		conf.OwnerList = &OwnerList{}
	}
	OwnerWhiteList = conf.OwnerList
	RepoWhiteList = conf.RepoList
	return nil
}
