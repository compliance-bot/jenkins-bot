package config

import "time"

type JsonBody struct {
	Action         string        `json:"action"`
	ActionDesc     interface{}   `json:"action_desc"`
	PullRequest    PullRequest   `json:"pull_request"`
	Number         int           `json:"number"`
	Iid            int           `json:"iid"`
	Title          string        `json:"title"`
	Body           string        `json:"body"`
	Languages      []interface{} `json:"languages"`
	State          string        `json:"state"`
	MergeStatus    string        `json:"merge_status"`
	MergeCommitSha interface{}   `json:"merge_commit_sha"`
	URL            string        `json:"url"`
	SourceBranch   string        `json:"source_branch"`
	SourceRepo     SourceRepo    `json:"source_repo"`
	TargetBranch   string        `json:"target_branch"`
	TargetRepo     TargetRepo    `json:"target_repo"`
	Project        Project       `json:"project"`
	Repository     Repository    `json:"repository"`
	Author         Author        `json:"author"`
	UpdatedBy      UpdatedBy     `json:"updated_by"`
	Sender         Sender        `json:"sender"`
	TargetUser     interface{}   `json:"target_user"`
	Enterprise     interface{}   `json:"enterprise"`
	HookName       string        `json:"hook_name"`
	HookID         int           `json:"hook_id"`
	HookURL        string        `json:"hook_url"`
	Password       string        `json:"password"`
	Timestamp      string        `json:"timestamp"`
	Sign           string        `json:"sign"`
}
type User struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Username  string `json:"username"`
	UserName  string `json:"user_name"`
	URL       string `json:"url"`
	Login     string `json:"login"`
	AvatarURL string `json:"avatar_url"`
	HTMLURL   string `json:"html_url"`
	Type      string `json:"type"`
	SiteAdmin bool   `json:"site_admin"`
}
type Owner struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Username  string `json:"username"`
	UserName  string `json:"user_name"`
	URL       string `json:"url"`
	Login     string `json:"login"`
	AvatarURL string `json:"avatar_url"`
	HTMLURL   string `json:"html_url"`
	Type      string `json:"type"`
	SiteAdmin bool   `json:"site_admin"`
}
type Repo struct {
	ID                int         `json:"id"`
	Name              string      `json:"name"`
	Path              string      `json:"path"`
	FullName          string      `json:"full_name"`
	Owner             Owner       `json:"owner"`
	Private           bool        `json:"private"`
	HTMLURL           string      `json:"html_url"`
	URL               string      `json:"url"`
	Description       string      `json:"description"`
	Fork              bool        `json:"fork"`
	CreatedAt         time.Time   `json:"created_at"`
	UpdatedAt         time.Time   `json:"updated_at"`
	PushedAt          time.Time   `json:"pushed_at"`
	GitURL            string      `json:"git_url"`
	SSHURL            string      `json:"ssh_url"`
	CloneURL          string      `json:"clone_url"`
	SvnURL            string      `json:"svn_url"`
	GitHTTPURL        string      `json:"git_http_url"`
	GitSSHURL         string      `json:"git_ssh_url"`
	GitSvnURL         string      `json:"git_svn_url"`
	Homepage          interface{} `json:"homepage"`
	StargazersCount   int         `json:"stargazers_count"`
	WatchersCount     int         `json:"watchers_count"`
	ForksCount        int         `json:"forks_count"`
	Language          string      `json:"language"`
	HasIssues         bool        `json:"has_issues"`
	HasWiki           bool        `json:"has_wiki"`
	HasPages          bool        `json:"has_pages"`
	License           string      `json:"license"`
	OpenIssuesCount   int         `json:"open_issues_count"`
	DefaultBranch     string      `json:"default_branch"`
	Namespace         string      `json:"namespace"`
	NameWithNamespace string      `json:"name_with_namespace"`
	PathWithNamespace string      `json:"path_with_namespace"`
}
type Head struct {
	Label string `json:"label"`
	Ref   string `json:"ref"`
	Sha   string `json:"sha"`
	User  User   `json:"user"`
	Repo  Repo   `json:"repo"`
}
type Base struct {
	Label string `json:"label"`
	Ref   string `json:"ref"`
	Sha   string `json:"sha"`
	User  User   `json:"user"`
	Repo  Repo   `json:"repo"`
}
type UpdatedBy struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Username  string `json:"username"`
	UserName  string `json:"user_name"`
	URL       string `json:"url"`
	Login     string `json:"login"`
	AvatarURL string `json:"avatar_url"`
	HTMLURL   string `json:"html_url"`
	Type      string `json:"type"`
	SiteAdmin bool   `json:"site_admin"`
}
type PullRequest struct {
	ID                 int           `json:"id"`
	Number             int           `json:"number"`
	State              string        `json:"state"`
	HTMLURL            string        `json:"html_url"`
	DiffURL            string        `json:"diff_url"`
	PatchURL           string        `json:"patch_url"`
	Title              string        `json:"title"`
	Body               string        `json:"body"`
	Labels             []interface{} `json:"labels"`
	Languages          []interface{} `json:"languages"`
	CreatedAt          time.Time     `json:"created_at"`
	UpdatedAt          time.Time     `json:"updated_at"`
	ClosedAt           interface{}   `json:"closed_at"`
	MergedAt           interface{}   `json:"merged_at"`
	MergeCommitSha     interface{}   `json:"merge_commit_sha"`
	MergeReferenceName string        `json:"merge_reference_name"`
	User               User          `json:"user"`
	Assignee           interface{}   `json:"assignee"`
	Assignees          []interface{} `json:"assignees"`
	Tester             interface{}   `json:"tester"`
	Testers            []interface{} `json:"testers"`
	NeedTest           bool          `json:"need_test"`
	NeedReview         bool          `json:"need_review"`
	Milestone          interface{}   `json:"milestone"`
	Head               Head          `json:"head"`
	Base               Base          `json:"base"`
	Merged             bool          `json:"merged"`
	Mergeable          bool          `json:"mergeable"`
	MergeStatus        string        `json:"merge_status"`
	UpdatedBy          UpdatedBy     `json:"updated_by"`
	Comments           int           `json:"comments"`
	Commits            int           `json:"commits"`
	Additions          int           `json:"additions"`
	Deletions          int           `json:"deletions"`
	ChangedFiles       int           `json:"changed_files"`
}
type Project struct {
	ID                int         `json:"id"`
	Name              string      `json:"name"`
	Path              string      `json:"path"`
	FullName          string      `json:"full_name"`
	Owner             Owner       `json:"owner"`
	Private           bool        `json:"private"`
	HTMLURL           string      `json:"html_url"`
	URL               string      `json:"url"`
	Description       string      `json:"description"`
	Fork              bool        `json:"fork"`
	CreatedAt         time.Time   `json:"created_at"`
	UpdatedAt         time.Time   `json:"updated_at"`
	PushedAt          time.Time   `json:"pushed_at"`
	GitURL            string      `json:"git_url"`
	SSHURL            string      `json:"ssh_url"`
	CloneURL          string      `json:"clone_url"`
	SvnURL            string      `json:"svn_url"`
	GitHTTPURL        string      `json:"git_http_url"`
	GitSSHURL         string      `json:"git_ssh_url"`
	GitSvnURL         string      `json:"git_svn_url"`
	Homepage          interface{} `json:"homepage"`
	StargazersCount   int         `json:"stargazers_count"`
	WatchersCount     int         `json:"watchers_count"`
	ForksCount        int         `json:"forks_count"`
	Language          string      `json:"language"`
	HasIssues         bool        `json:"has_issues"`
	HasWiki           bool        `json:"has_wiki"`
	HasPages          bool        `json:"has_pages"`
	License           string      `json:"license"`
	OpenIssuesCount   int         `json:"open_issues_count"`
	DefaultBranch     string      `json:"default_branch"`
	Namespace         string      `json:"namespace"`
	NameWithNamespace string      `json:"name_with_namespace"`
	PathWithNamespace string      `json:"path_with_namespace"`
}
type Repository struct {
	ID                int         `json:"id"`
	Name              string      `json:"name"`
	Path              string      `json:"path"`
	FullName          string      `json:"full_name"`
	Owner             Owner       `json:"owner"`
	Private           bool        `json:"private"`
	HTMLURL           string      `json:"html_url"`
	URL               string      `json:"url"`
	Description       string      `json:"description"`
	Fork              bool        `json:"fork"`
	CreatedAt         time.Time   `json:"created_at"`
	UpdatedAt         time.Time   `json:"updated_at"`
	PushedAt          time.Time   `json:"pushed_at"`
	GitURL            string      `json:"git_url"`
	SSHURL            string      `json:"ssh_url"`
	CloneURL          string      `json:"clone_url"`
	SvnURL            string      `json:"svn_url"`
	GitHTTPURL        string      `json:"git_http_url"`
	GitSSHURL         string      `json:"git_ssh_url"`
	GitSvnURL         string      `json:"git_svn_url"`
	Homepage          interface{} `json:"homepage"`
	StargazersCount   int         `json:"stargazers_count"`
	WatchersCount     int         `json:"watchers_count"`
	ForksCount        int         `json:"forks_count"`
	Language          string      `json:"language"`
	HasIssues         bool        `json:"has_issues"`
	HasWiki           bool        `json:"has_wiki"`
	HasPages          bool        `json:"has_pages"`
	License           string      `json:"license"`
	OpenIssuesCount   int         `json:"open_issues_count"`
	DefaultBranch     string      `json:"default_branch"`
	Namespace         string      `json:"namespace"`
	NameWithNamespace string      `json:"name_with_namespace"`
	PathWithNamespace string      `json:"path_with_namespace"`
}
type SourceRepo struct {
	Project    Project    `json:"project"`
	Repository Repository `json:"repository"`
}
type TargetRepo struct {
	Project    Project    `json:"project"`
	Repository Repository `json:"repository"`
}
type Author struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Username  string `json:"username"`
	UserName  string `json:"user_name"`
	URL       string `json:"url"`
	Login     string `json:"login"`
	AvatarURL string `json:"avatar_url"`
	HTMLURL   string `json:"html_url"`
	Type      string `json:"type"`
	SiteAdmin bool   `json:"site_admin"`
}
type Sender struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Username  string `json:"username"`
	UserName  string `json:"user_name"`
	URL       string `json:"url"`
	Login     string `json:"login"`
	AvatarURL string `json:"avatar_url"`
	HTMLURL   string `json:"html_url"`
	Type      string `json:"type"`
	SiteAdmin bool   `json:"site_admin"`
}
