module gitee.com/compliance-bot/jenkins-bot

go 1.15

require (
	gitee.com/openeuler/go-gitee v0.0.0-20210427125813-de0b78ea30e9
	github.com/go-yaml/yaml v2.1.0+incompatible
)
