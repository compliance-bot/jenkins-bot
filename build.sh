#!/bin/sh

mkdir -p /var/jenkins_home/gowork

export GOPATH=/var/jenkins_home/gowork
export PATH=$GOPATH:$PATH
echo "GOPATH: ${GOPATH}"
go version

if [ "${giteePullRequestState}" = "open" ]; then
     echo "PR states is open, later will begin license scanning"
else
     exit 0
fi

export 'GO111MODULE=on'
export 'GOPROXY=https://goproxy.io'

cd ${GOPATH}
ls

GITEE_TOKEN="your_token"


mkdir -p /var/jenkins_home/gowork/src/gitee.com/compliance-bot/
cd /var/jenkins_home/gowork/src/gitee.com/compliance-bot/
rm -rf jenkins-bot*

git clone ${GIT_URL}
cd jenkins-bot
go mod download
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 && go build check.go

# license check
./check licenseScan  ${giteePullRequestIid} ${giteeSourceBranch} ${giteeSourceRepoHttpUrl} ${GITEE_TOKEN} ${giteeTargetNamespace} ${giteeTargetRepoName}