package repo

import (
	"gitee.com/compliance-bot/jenkins-bot/config"
	"log"
)

func WhiteListRepo(owner, repo string) bool {
	err := config.LoadConfig("./config/conf/repo.yaml")
	if err != nil {
		log.Fatalf("load config file faild: %s", err)
	}

	ownerWhiteList := config.OwnerWhiteList.Owners
	repoWhiteList := config.RepoWhiteList.Repos

	if IsInStringSlice(owner, ownerWhiteList) && IsInStringSlice(repo, repoWhiteList) {
		return true
	}
	return false
}

func IsInStringSlice(str string, whiteList []string) bool {
	for _, s := range whiteList {
		if s == str {
			return true
		}
	}
	return false
}
