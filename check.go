package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/compliance-bot/jenkins-bot/config"
	"gitee.com/compliance-bot/jenkins-bot/repo"
	sdk "gitee.com/openeuler/go-gitee/gitee"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	targetOwner string
	targetRepo  string

	sourceBranch string
	sourceRepo   string

	token string
	prIID string
)

func main() {
	processArgs(os.Args)
}

func processArgs(args []string) {
	// no valid args
	if len(args) > 8 {
		log.Fatalln("args error")
	}

	prIID = args[2]
	sourceBranch = args[3]
	sourceRepo = args[4]
	token = args[5]

	targetOwner = args[6]
	targetRepo = args[7]

	if !repo.WhiteListRepo(targetOwner, targetRepo) {
		return
	}

	// proceed by args
	switch args[1] {
	case "licenseScan":
		go licenseCheck()
	case "copyrightScan":
		help()
	case "-h":
		help()
	default:
		help()
	}
}

func help() {
	fmt.Printf(`giteeCheck - used for open source project ci flow Jenkins script

Args:
	licenseScan ${giteePullRequestIid} ${sourceBranch} ${sourceRepo} add licenses scan results once PR created
	-h	Help and quit

`)
	os.Exit(0)
}

func licenseCheck() {
	// todo welcome msg
	url := "https://compliance.openeuler.org/ci"
	pr := &config.PullRepo{
		Action: "license_scan_general",
		Repo:   sourceRepo,
		Branch: sourceBranch,
	}

	fmt.Println("pr: ", pr)
	bytesPr, err := json.Marshal(pr)
	if err != nil {
		fmt.Println("send pull request params error: " + err.Error())
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	req, err := http.NewRequest("POST", url, bytes.NewReader(bytesPr))
	if err != nil {
		fmt.Println("request to compliance tool error:" + err.Error())
		return
	}
	req.Header.Set("Content-Type", "application/json;charset=UTF-8")

	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		fmt.Println("response from compliance tool error: " + err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("comment parse error:" + err.Error())
		return
	}

	var sc config.ScanResult
	err = json.Unmarshal(body, &sc)
	if err != nil {
		fmt.Println("parse info from compliance error:" + err.Error())
		return
	}

	go resultBack(sc)
}

func resultBack(sc config.ScanResult) {
	comments := fmt.Sprintf("The license and copright scaning is processing, " + sc.ReportSummary + ", you can click " + sc.ReportUrl + " to watch the report.")
	opt := sdk.PullRequestCommentPostParam{
		AccessToken: token,
		Body:        comments,
	}
	commentUrl := fmt.Sprintf("https://gitee.com/api/v5/repos/%s/%s/pulls/%s/comments", targetOwner, targetRepo, prIID)

	byteOpt, err := json.Marshal(opt)
	if err != nil {
		fmt.Println("comment parse error:" + err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	req, err := http.NewRequest("POST", commentUrl, bytes.NewReader(byteOpt))
	if err != nil {
		fmt.Println("request to comment error:" + err.Error())
		return
	}
	req.Header.Set("Content-Type", "application/json;charset=UTF-8")

	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		fmt.Println("response to comment error: " + err.Error())
		return
	}
	fmt.Println("status:", resp.StatusCode)
	defer resp.Body.Close()
}

func hasComments(prIID string) bool {
	url := fmt.Sprintf("https://gitee.com/api/v5/repos/%s/%s/pulls/%s/comments?page=1&per_page=5", targetOwner, targetRepo, prIID)
	response, _ := http.Get(url)
	defer response.Body.Close()

	body, _ := ioutil.ReadAll(response.Body)

	var result []interface{}
	json.Unmarshal(body, &result)
	if len(result) == 0 {
		return true
	}
	return false
}
